// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use clap::{Arg, ArgAction, ArgMatches, Command};
use comfy_table::{Cell, Row};
use gitlab_runner_api::RunnerDetails;
use thiserror::Error;

mod display;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Column {
    Id,
    Name,
    State,
    Type,
    Shared,
    Description,
    Tags,
    IpAddress,
    Platform,
    Architecture,
    Revision,
    AccessLevel,
    MaximumTimeout,
}

impl Column {
    fn need_details(self) -> bool {
        matches!(self, Self::Tags | Self::Platform | Self::Architecture | Self::Revision | Self::AccessLevel | Self::MaximumTimeout)
    }

    fn opt_string_or_default<'a>(opt: &'a Option<String>, dflt: &'static str) -> &'a str {
        opt.as_ref().map(AsRef::as_ref).unwrap_or(dflt)
    }

    fn cell_for(self, details: &RunnerDetails) -> Cell {
        match self {
            Self::Id => Cell::new(details.id),
            Self::Name => Cell::new(Self::opt_string_or_default(&details.name, "<unnamed>")),
            Self::State => Cell::new(display::RunnerState::new(details)),
            Self::Type => Cell::new(display::runner_type(details.runner_type)),
            Self::Shared => Cell::new(display::BoolMarkup::new(details.is_shared)),
            Self::Description => Cell::new(Self::opt_string_or_default(&details.description, "<no description>")),
            Self::Tags => Cell::new(details.tags.join(",")),
            Self::IpAddress => Cell::new(Self::opt_string_or_default(&details.ip_address, "<no IP>")),
            Self::Platform => Cell::new(Self::opt_string_or_default(&details.platform, "<unknown>")),
            Self::Architecture => Cell::new(Self::opt_string_or_default(&details.architecture, "<unknown>")),
            Self::Revision => Cell::new(Self::opt_string_or_default(&details.revision, "<unknown>")),
            Self::AccessLevel => Cell::new(display::runner_access_level(details.access_level)),
            Self::MaximumTimeout => Cell::new(display::runner_maximum_timeout(details.maximum_timeout)),
        }
    }

    fn header(self) -> &'static str {
        match self {
            Self::Id => "ID",
            Self::Name => "Name",
            Self::State => "State",
            Self::Type => "Type",
            Self::Shared => "Shared",
            Self::Description => "Description",
            Self::Tags => "Tags",
            Self::IpAddress => "IP",
            Self::Platform => "Platform",
            Self::Architecture => "Architecture",
            Self::Revision => "Revision",
            Self::AccessLevel => "Access",
            Self::MaximumTimeout => "Timeout",
        }
    }

    fn name(self) -> &'static str {
        match self {
            Self::Id => "id",
            Self::Name => "name",
            Self::State => "state",
            Self::Type => "type",
            Self::Shared => "shared",
            Self::Description => "description",
            Self::Tags => "tags",
            Self::IpAddress => "ip",
            Self::Platform => "platform",
            Self::Architecture => "architecture",
            Self::Revision => "revision",
            Self::AccessLevel => "access",
            Self::MaximumTimeout => "timeout",
        }
    }
}

#[derive(Debug, Clone)]
pub struct Columns {
    columns: Vec<Column>,
}

impl Columns {
    pub fn headers(&self) -> Row {
        self.columns
            .iter()
            .map(|col| col.header())
            .collect::<Vec<_>>()
            .into()
    }

    pub fn need_details(&self) -> bool {
        self.columns.iter().any(|col| col.need_details())
    }

    pub fn row_for(&self, overview: &RunnerDetails) -> Row {
        let mut row = Row::new();

        self.columns
            .iter()
            .for_each(|col| {
                row.add_cell(col.cell_for(overview));
            });

        row
    }
}

impl Default for Columns {
    fn default() -> Self {
        Self {
            columns: vec![
                Column::Id,
                Column::Name,
                Column::State,
                Column::Type,
                Column::Shared,
                Column::Description,
            ],
        }
    }
}

pub struct ColumnOptions;

#[derive(Debug, Error)]
enum ColumnParseError {
    #[error("unrecognized column: {}", column)]
    UnrecognizedColumn {
        column: String,
    },
}

impl ColumnParseError {
    fn unrecognized_column(column: &str) -> Self {
        Self::UnrecognizedColumn {
            column: column.into(),
        }
    }
}

macro_rules! column_pairs{
    [$($col:expr,)*] => {
        [$(($col, $col.name()),)*]
    };
}

impl ColumnOptions {
    fn column_parser(arg: &str) -> Result<Columns, ColumnParseError> {
        let all_columns = column_pairs![
            Column::Id,
            Column::Name,
            Column::State,
            Column::Type,
            Column::Shared,
            Column::Description,
            Column::Tags,
            Column::IpAddress,
            Column::Platform,
            Column::Architecture,
            Column::Revision,
            Column::AccessLevel,
            Column::MaximumTimeout,
        ];

        let columns = arg.split(',')
            .map(|component| {
                let (column, _) = all_columns
                    .iter()
                    .find(|(_, name)| *name == component)
                    .ok_or_else(|| ColumnParseError::unrecognized_column(component))?;

                Ok(*column)
            })
            .collect::<Result<Vec<_>, _>>()?;

        // TODO: find and reject duplicate columns

        Ok(Columns {
            columns,
        })
    }

    /// Add filter options to a command.
    pub fn add_options(cmd: Command) -> Command {
        cmd.arg(
            Arg::new("COLUMNS")
                .short('C')
                .long("columns")
                .value_parser(Self::column_parser)
                .help("Column expression")
                .value_name("COLUMNS")
                .action(ArgAction::Set),
        )
    }

    /// Build all provided filter options into a filter.
    pub fn build(matches: &ArgMatches) -> Columns {
        matches.get_one("COLUMNS")
            .cloned()
            .unwrap_or_else(Columns::default)
    }
}
