// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![deny(missing_docs)]

//! GitLab Runner API
//!
//! This crate provides abstractions to query and manage runners on a GitLab instance.

pub mod actions;
mod data;
mod runner;

/// The `gitlab` crate used.
pub use gitlab;

pub use data::Commit;
pub use data::Job;
pub use data::Pipeline;
pub use data::Project;
pub use data::RunnerAccessLevel;
pub use data::RunnerDetails;
pub use data::RunnerJobStatus;
pub use data::RunnerOverview;
pub use data::RunnerToken;
pub use data::RunnerType;

pub use runner::Runner;
